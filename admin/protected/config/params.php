<?php

return array(
	'url.format' => 'path',
	'url.rules' => array(
		/* for REST please @see http://www.yiiframework.com/wiki/175/how-to-create-a-rest-api/ */
		/* other @see http://www.yiiframework.com/doc/guide/1.1/en/topics.url */
		'<controller:\w+>/<id:\d+>' => '<controller>/view',
		'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
		'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
	),
);