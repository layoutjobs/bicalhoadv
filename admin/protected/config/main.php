<?php

$base 		    = __DIR__ . '/..';
$common 		= __DIR__ . '/../../../common';
$environment 	= APP_ENV;

$params         = require($base . '/config/params-' . $environment . '.php');
$commonParams   = require($common . '/config/params-' . $environment . '.php');

Yii::setPathOfAlias('common', $common);

return CMap::mergeArray(array(
	'name' 					=> 'Admin',
	'defaultController' 	=> 'home', 
	'language' 				=> 'pt_br',
	'timeZone'              => 'America/Sao_Paulo', 

	'aliases' => array(
		'bootstrap' => 'common.extensions.yiistrap',
    ),
	
	'preload' => array(
		'log',
		'bootstrap',
	),
	
	'import' => array(
        'common.components.*',
        'common.extensions.*',
        'common.models.*',
        'application.components.*',
        'application.extensions.*',
        'application.models.*',
        'bootstrap.helpers.TbHtml',
	),
	
	'modules' => array(

	),
	
	'components' => array(
		'coreMessages' => array(
			'basePath' => $common . '/messages', 
		),
		'user' => array(
			'loginUrl' => array('home/login'),
			'allowAutoLogin' => true,
		),
		'db' => array(
			'connectionString' => 'sqlite:../common/data/database.db',
			'tablePrefix' => 'tbl_',
		),
		'urlManager' => array(
			'urlFormat' => $params['url.format'],
			'rules' => $params['url.rules'],
			'urlSuffix' => '/',
			'showScriptName' => false, // requer .htaccess
		),
		'errorHandler' => array(
			'errorAction' => 'home/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),
	),
	
	'params' => CMap::mergeArray($commonParams, $params),
), require($base . '/config/main-' . $environment . '.php'));