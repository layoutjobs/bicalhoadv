<?php

class Controller extends CController
{	
	private $_model;

	public $pageSubtitle;
	
	public $menu = array();

	public $breadcrumbs = array();

	public $searchForm = false;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'users' => array('@'),
			),
			array('deny'),
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel($modelClass, $id = null)
	{
		if ($this->_model === null) {
			$model = new $modelClass;
			if ($id || isset($_GET['id']))
				$this->_model = $model->findByPk($id ? $id : $_GET['id']);

			if ($this->_model === null)
				throw new CHttpException(404,'A página solicitada não existe.');
		}
		return $this->_model;
	}
}
