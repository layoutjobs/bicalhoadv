<?php

class UserController extends Controller
{	
	public $searchForm = true;
		
	public function accessRules()
	{
		return CMap::mergeArray(array(
			array(
				'allow',
				'actions' => array('alterarSenha'),
				'users' => array('@'),
			),
		), parent::accessRules());
	}
	
	public function actionIndex($s = null)
	{
		$model = new User('search');
		$model->unsetAttributes();

		if (isset($_GET['User']))
			$model->setAttributes($_GET['User']);
		else 
			$model->s = $s;
		
		$this->render('index', array('model' => $model));
	}
	
	public function actionCreate()
	{
		$model = new User;

		if(isset($_POST['User'])) {
			$model->setAttributes($_POST['User']);

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel('User');

		if (isset($_POST['User'])) {
			$model->setAttributes($_POST['User']);

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->refresh();
			}
		}

		$this->render('update', array('model' => $model));
	}
	
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel('User', $id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!Yii::app()->request->isAjaxRequest) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> O registro foi excluído com sucesso.');	

				$this->redirect(array('index'));
			}
		} else
			throw new CHttpException(400, 'Requisição inválida. Por favor, não repita esta requisição novamente.');
	}
}
