<?php

class PostController extends Controller
{		
	public $searchForm = true;

	public function actions()
	{
		return array(
			'upload' => array(
				'class' => 'application.actions.UploadAction',
				'path' => Post::model()->getUploadBasePath('img'),
				'url' => Post::model()->getUploadBaseUrl('img'),
				'options' => array(
					'accept_file_types' => Post::model()->getUploadScopeAcceptFileTypes('img'),
					'image_versions' => array(		
						'thumbnail' => array(
							'max_width' => 140,
							'max_height' => 210,
						),
					),
				),
			),
		);
	}
	
	public function actionIndex($s = null)
	{
		$model = new Post('search');
		$model->unsetAttributes();
		
		if (isset($_GET['Post']))
			$model->setAttributes($_GET['Post']);
		else 
			$model->s = $s;
		
		$this->render('index', array('model' => $model));
	}
	
	public function actionCreate()
	{
		$model = new Post;

		if(isset($_POST['Post'])) {
			$model->setAttributes($_POST['Post']);

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel('Post');

		if (isset($_POST['Post'])) {
			$model->setAttributes($_POST['Post']);

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->refresh();
			}
		}

		$this->render('update', array('model' => $model));
	}
	
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel('Post', $id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!Yii::app()->request->isAjaxRequest) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> O registro foi excluído com sucesso.');		

				$this->redirect(array('index'));
			}
		} else
			throw new CHttpException(400, 'Requisição inválida. Por favor, não repita esta requisição novamente.');
	}

	public function actionChangeImg($id, $img)
	{
		$model = $this->loadModel('Post', $id);	
		$model->img = $img;

		if ($model->save())
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
				'<strong>Sucesso!</strong> A imagem foi alterada com sucesso.');		

		$this->redirect($this->createUrl('update', array('id' => $model->id)));
	}

	public function actionListImg($id)
	{
		$model = $this->loadModel('Post');
		$imgs = array();

		foreach ($model->getFiles('img') as $i => $file) {
			$obj = new stdClass();
			$obj->image = $file->url;
			$obj->title = $file->name;
			$imgs[$i] = $obj;
		}	

		foreach ($model->getFiles('img', 'thumbnail') as $i => $file) {
			$imgs[$i]->thumb = $file->url;
		}

		echo CJSON::encode($imgs);
	}
}
