<?php

class HomeController extends Controller
{
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('index', 'logout', 'error'),
				'users' => array('@'),
			),
			array(
				'allow',
				'actions' => array('error', 'login'),
				'users' => array('*'),
			),
		);
	}	
		
	public function actionIndex()
	{
		$this->redirect(Yii::app()->createUrl('post'));
	}
	
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionLogin()
	{
		$model = new LoginForm;
		
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'loginForm') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		
		$this->render('login', array('model' => $model));
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
