<?php 
$this->pageTitle = 'Posts';
$this->menu = array(
	array(
		array(
			'label' => 'Incluir',
			'icon' => 'file',
			'url' => array('create'),
		),
	),
); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'postGrid',
	'type' => array(TbHtml::GRID_TYPE_STRIPED, TbHtml::GRID_TYPE_BORDERED),
	'dataProvider' => $model->search(),
	'columns' => array(
		'id',
		array(
			'name' => 'status',
			'value' => 'Lookup::item("PostStatus", $data->status)',
			'filter' => Lookup::items('PostStatus'),
		),
		'title',
		'tags',
		'categories',
		array(
			'name' => 'update_time',
			'value' => 'date("d/m/Y h:m", $data->update_time)',
		),	
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => "{update}\n{delete}",
		),
	),
)); ?>