<?php $files = $model->getFiles('img', 'thumbnail'); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'userForm',
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php ob_start(); ?>

<div class="control-group">
	<?php echo $form->labelEx($model, 'img', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php if ($files) : ?>
		<ul class="thumbnails inline" style="margin-bottom: 0;">
			<?php foreach ($files as $file) : ?>		
			<li>
				<?php if ($model->img === $file->name) : ?>
				<?php echo CHtml::link($file->render, $this->createUrl('changeImg', array(
					'id' => $model->id,
					'img' => '',
				)), array('class' => 'thumbnail active')); ?>
				<?php else : ?>
				<?php echo CHtml::link($file->render, $this->createUrl('changeImg', array(
					'id' => $model->id,
					'img' => $file->name,
				)), array('class' => 'thumbnail')); ?>
				<?php endif; ?>
			</li>
			<?php endforeach; ?>
		</ul>
		<p class="help-inline">Clique sobre uma imagem para defini-la como principal.</p>
		<?php elseif ($model->isNewRecord) : ?>
		<p class="alert alert-info" style="margin-bottom: 0;">Salve o registro para habilitar o envio de imagens.</p>
		<?php else : ?>
		<p class="alert alert-info" style="margin-bottom: 0;">Clique em Upload para enviar imagens.</p>	
		<?php endif; ?>
		<?php echo $form->error($model, 'img'); ?>
	</div>
</div>	

<?php echo $form->dropDownListControlGroup($model, 'status', Lookup::items('PostStatus')); ?>

<?php echo $form->textFieldControlGroup($model, 'categories', array(
	'data-mode' => 'multiple',
	'data-provide' => 'typeahead',
	'data-source' => CJSON::encode(Category::model()->suggestCategories('Post', '', -1)),
	'autocomplete' => 'off',
)); ?>

<?php echo $form->textFieldControlGroup($model, 'title'); ?>

<div class="control-group">
	<?php echo $form->labelEx($model, 'content', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php $this->widget('ext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
		    'model' => $model,
		    'attribute' => 'content',
		    'options' => array(
		        'lang' => Yii::app()->language,
				'imageGetJson' => Yii::app()->createUrl('post/listImg', array('id' => $model->id)),
		    ),
		)); ?>
		<?php echo $form->error($model, 'content'); ?>
	</div>
</div>

<div class="form-actions">
	<?php echo TbHtml::button('Salvar', array(
		'type' => 'submit',
		'color' => TbHtml::BUTTON_COLOR_PRIMARY,
		'icon' => TbHtml::ICON_OK,
	)); ?>	

	<?php echo TbHtml::linkButton('Cancelar', array(
		'type' => 'link',
		'url' => array('index'),
		'color' => TbHtml::BUTTON_COLOR_LINK,
	)); ?>
</div>
	
<?php $this->endWidget(); ?>

<?php $tabs[] = array(
	'label' => 'Post' . ($model->hasErrors() ? ' ' . TbHtml::tag('span', array('class' => 'badge badge-important'), '!') : ''),
	'content' => ob_get_contents(),
	'active' => true,
); ob_end_clean(); ?>

<?php $tabs[] = array(
	'label' => 'Upload',
	'visible' => !$model->isNewRecord,
	'content' => $this->renderPartial('_upload', array('model' => $model), true),
); ?>

<?php $this->widget('bootstrap.widgets.TbTabs', array(
	'id' => 'postFormTabs',
	'tabs' => $tabs,
)); ?>

<?php 
$js = <<<EOD
$('#postFormTabs .nav > li:first > a').on('click', function () {
	location.reload()
})
EOD;

Yii::app()->clientScript->registerScript('postForm', $js); ?>