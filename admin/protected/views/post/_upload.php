<p><i class="icon icon-info-sign"></i> Evite enviar images com nomes contendo espaços, acentos ou 
caracteres especiais (Ç, !, ?, etc.), assim como nomes muito  longos (máximo 128 caracteres). 
São aceitos arquivos com as seguintes extensões: <strong>.gif, .jpg e .png</strong>.</p> 

<?php $this->widget('application.widgets.Upload', array(
	'id' => 'post',
	'url' => $this->createUrl('upload', array('folder' => $model->getUploadModelFolder())),
	/*
	'options' => array(
		'maxNumberOfFiles' => 1,
	),
	*/
)); ?>

