<?php $this->pageTitle = 'Login'; ?>	

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'loginForm',
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
	'focus' => array($model, 'username'),
)); ?>

<div class="page-header text-center">
	<h1><?php echo $this->pageTitle; ?></h1>
</div>

<?php echo $form->errorSummary($model); ?>

<div class="panel">
	<?php echo $form->labelEx($model, 'username'); ?>

	<?php echo $form->textField($model, 'username', array('required' => 'required')); ?>
	
	<span class="hide"><?php echo $form->error($model, 'username'); ?></span>

	<?php echo $form->labelEx($model, 'password'); ?>

	<?php echo $form->passwordField($model, 'password', array('required' => 'required')); ?>
	
	<span class="hide"><?php echo $form->error($model, 'password'); ?></span>

	<div class="form-actions">
	 	<label class="checkbox pull-left">
	    	<?php echo $form->checkBox($model, 'rememberMe'); ?>
	    	<?php echo $model->getAttributeLabel('rememberMe'); ?>
		</label>

		<?php echo TbHtml::button('Entrar', array(
			'type' => 'submit',
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'class' => 'pull-right',
		)); ?>	
	</div>
</div>

<?php $this->endWidget(); ?>
