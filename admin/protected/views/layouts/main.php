<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="pt-br" />
	<title><?php echo Yii::app()->name; ?> | <?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/bootstrap.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/bootstrap-responsive.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/main.css'); ?>	

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body class="ctrl-<?php echo $this->id . ' act-' . $this->action->id; ?>">
	
<?php if (!Yii::app()->user->isGuest) : ?>
	
<?php $this->widget('bootstrap.widgets.TbNavbar', array(
	'brandLabel' => CHtml::encode(Yii::app()->name),
	'brandUrl' => Yii::app()->createUrl('home/index'),
	'color' => TbHtml::NAVBAR_COLOR_INVERSE,
	'collapse' => true,
	'items' => array(
		array(
			'class' => 'bootstrap.widgets.TbNav',
			'items' => array(
				array(
					'label' => 'Posts',
					'url' => array('/post'),
				),
				array(
					'label' => 'Usuários',
					'url' => array('/user'),
				),
			),
		),	
		array(
			'class' => 'bootstrap.widgets.TbNav',
			'htmlOptions' => array('class' => 'pull-right'),
			'items' => array(
				TbHtml::navbarMenuDivider(),	
				array(
					'icon' => TbHtml::ICON_COG,
					'items' => array(
						array(
							'label' => 'Perfil',
							'icon' => TbHtml::ICON_USER,
							'url' => array('/user/update', 'id' => Yii::app()->user->id),
						),
						TbHtml::menuDivider(),						
						array(
							'label' => 'Sair',
							'icon' => TbHtml::ICON_OFF,
							'url' => array('/home/logout'),
						),						
					),
				),
				TbHtml::navbarMenuDivider(),
			),
		),
	),
)); ?>
	
<div id="header">
	<div class="container">
		<h1 class="pull-left"><?php echo $this->pageTitle; ?> <small><?php echo $this->pageSubtitle; ?></small></h1>
		<?php if ($this->searchForm || $this->menu) : ?>		
		<div class="well pull-right">
			<?php if ($this->searchForm) {
				echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_SEARCH, $this->createUrl('index'), 'get');
				echo TbHtml::textField('s', Yii::app()->request->getParam('s'), 
					array('class' => 'search-query', 'placeholder' => 'Pesquisar', 'autocomplete' => 'false'));
				echo TbHtml::submitButton(TbHtml::icon(TbHtml::ICON_SEARCH, array('color' => TbHtml::ICON_COLOR_WHITE)), 
					array('color' => TbHtml::BUTTON_COLOR_LINK, 'name' => false));
				echo TbHtml::endForm();
			} 
			foreach ($this->menu as $items) {
				echo TbHtml::buttonToolbar(array(array(
					'items' => $items, 
					'color' => TbHtml::BUTTON_COLOR_INVERSE, 
				)));
			} ?>
		</div>
		<?php endif; ?>
	</div>
</div>

<?php endif; ?>

<div id="content">
	<div class="container">
		<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
		<?php echo $content; ?>
	</div>
</div>

<div id="ajax-loading"></div>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/main.js', CClientScript::POS_END); ?>

</body>
</html>
