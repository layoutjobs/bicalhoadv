<?php

include(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'UploadHandler.php';

class UploadAction extends CAction
{
	private $uploadHandler;
	
	public $options = array();
	
	public $path;
	
	public $url;
	
	public $folder;
	
	public function init()
	{
		if (!$this->folder)
			$this->folder = isset($_REQUEST['folder']) ? ($_REQUEST['folder']) . '/' : '';

		if (!$this->folder)
			$this->options['script_url']  = $this->controller->createUrl($this->controller->action->id);
		else
			$this->options['script_url']  = $this->controller->createUrl($this->controller->action->id, array(
				'folder' => $this->folder,
			));
		
		$this->options['upload_dir']  = $this->path . $this->folder;
		$this->options['upload_url']  = $this->url . $this->folder;
	
		$this->uploadHandler = new UploadHandler($this->options);
	}
	
	public function run()
	{
		$this->init();
	}
}