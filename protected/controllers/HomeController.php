<?php

class HomeController extends Controller
{	
	public function actionIndex()
	{
		$posts = Post::model()->findAll(array(
			'condition' => 'status = ' . Post::STATUS_PUBLISHED,
			'order' => 'update_time DESC',
			'limit' => 3,
		));

		$this->render('index', array('posts' => $posts));
	}

	public function actionContato()
	{
		$contatoForm = new ContatoForm;

		if (isset($_POST['ContatoForm'])) {
			$contatoForm->setAttributes($_POST['ContatoForm']);

			if ($contatoForm->validate() && $contatoForm->submit()) {
				Yii::app()->user->setFlash('success', 'Obrigado pelo contato. Responderemos a sua mensagem o mais breve possível.');
				$this->redirect(Yii::app()->homeUrl);				
			}
		}

		$this->render('contato', array('contatoForm' => $contatoForm));
	}
	
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

}
