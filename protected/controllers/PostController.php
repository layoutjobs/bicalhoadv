<?php

class PostController extends Controller
{			
	private $_model;

	public function actionIndex()
	{
		$criteria = new CDbCriteria(array(
			'condition' => 'status = ' . Post::STATUS_PUBLISHED,
			'order' => 'update_time DESC',
		));

		if (isset($_GET['category']))
			$criteria->addSearchCondition('categories', Category::unfriendlyName($_GET['category']));

		if (isset($_GET['tag']))
			$criteria->addSearchCondition('tags', Tag::unfriendlyName($_GET['tag']));

		if (isset($_GET['year']) && isset($_GET['month'])) {
			$timestamp = strtotime($_GET['year'] . '-' . $_GET['month'] . '-00');
			$criteria->compare('update_time', '> ' . $timestamp);
			$criteria->compare('update_time', '< ' . ($timestamp + 2629743), 'AND');
		}

		if (isset($_GET['name']))
			$criteria->addSearchCondition('name', $_GET['name']);

		if (isset($_GET['s'])) {
			$criteria->compare('title', $_GET['s'], true);
			$criteria->compare('content', $_GET['s'], true, 'OR');
		}

		$dataProvider = new CActiveDataProvider('Post', array(
			'pagination' => false,
			'criteria' => $criteria,
		));

		$this->render('index', array('dataProvider' => $dataProvider));
	}

	public function actionView($id)
	{
		$post = Post::model()->find(array(
			'condition' => 'id = :id OR name = :id',
			'params' => array(':id' => $id),
		));

		if ($post === null)
			$this->redirect($this->createUrl('/home'));

		$this->render('view', array('post' => $post));
	}

	public function renderCategoryNav()
	{
		$items = array();

		foreach (Category::model()->suggestCategories('Post', '') as $category) {
			$categoryName = Category::friendlyName($category);
			$items[] = array(
				'label' => CHtml::encode($category),
				'url' => $this->createUrl('/post/index', array('category' => $categoryName)),
				'active' => isset($_GET['category']) && $_GET['category'] === $categoryName,
			);
		}

		$this->renderPartial('_categoryNav', array('items' => $items));
	}

	public function renderArchiveNav()
	{
		$models = Post::model()->findAll(array(
			'condition' => 'status = ' . Post::STATUS_PUBLISHED,
			'order' => 'update_time DESC',
		));

		$dates = array();

		foreach ($models as $model) {
			$date = date('Y-m-01', $model->update_time);
			if (!in_array($date, $dates))
				$dates[] = $date;
		}

		$items = array();

		foreach ($dates as $date) {
			$dateExploded = explode('-', $date);
			$year = $dateExploded[0];
			$month = $dateExploded[1];
			$items[] = array(
				'label' => ucfirst(Yii::app()->dateFormatter->format('MMMM', $date)) . '/' . $year,
				'url' => $this->createUrl('/post/index', array(
					'year' => $year,
					'month' => $month,
				)),
				'active' => isset($_GET['year']) && isset($_GET['month'])
					&& $_GET['year'] === $year && $_GET['month'] === $month,
			);
		}

		$this->renderPartial('_archiveNav', array('items' => $items));
	}
}
