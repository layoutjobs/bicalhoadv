<?php

class ContatoForm extends CFormModel
{
	public $nome;
	public $telefone;
	public $email;
	public $mensagem;

	public function rules()
	{
		return array(
			array('nome, email, mensagem', 'required'),
			array('email', 'email'),
			array('telefone', 'safe'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'nome' => 'Seu nome',
			'telefone' => 'Telefone',
			'email' => 'Seu e-mail',	
			'mensagem' => 'Mensagem',
		);
	}

	public function submit()
	{
		$from = Yii::app()->params['mail.from'];
		$from = 'contato@bicalhoadv1.hospedagemdesites.ws';

		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=utf-8\r\n";
		$headers .= "From: {$from}\r\n";
		$headers .= "Reply-To: {$this->email}\r\n";
		$headers .= "Return-Path: {$from}\r\n";
		$body  = "<style>";
		$body .= "table { width: 100%; font: normal 13px/20px Arial, sans-serif; }";
		$body .= "th, td { padding: 4px 8px; border-bottom: 1px solid #ddd; }";
		$body .= "th { width: 140px; text-align: left; }";
		$body .= "</style>";
		$body .= "<table>";
		$body .= "<tr>";
		$body .= "<th>{$this->getAttributeLabel('nome')}:</th>";
		$body .= "<td>{$this->nome}</td>";
		$body .= "</tr><tr>";
		$body .= "<th>{$this->getAttributeLabel('telefone')}:</th>";
		$body .= "<td>{$this->telefone}</td>";
		$body .= "</tr><tr>";
		$body .= "<th>{$this->getAttributeLabel('email')}:</th>";
		$body .= "<td>{$this->email}</td>";
		$body .= "</tr><tr>";
		$body .= "<th>{$this->getAttributeLabel('mensagem')}:</th>";
		$body .= "<td>{$this->mensagem}</td>";
		$body .= "</tr>";
		$body .= "</table>";
		
		return mail($from, 'Site > Contato', $body, $headers, "-r" . $from);
	}
}
