<?php $this->pageTitle = 'Contato'; ?>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<?php echo $this->renderPartial('/home/_contatoForm', array('contatoForm' => $contatoForm)); ?>
	</div>
</div>