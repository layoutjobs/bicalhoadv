<?php 
$contatoForm = isset($contatoForm) ? $contatoForm : new ContatoForm;
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'contatoForm',
	'method' => 'post',
	'action' => array('/home/contato'),
)); 
?>

<?php echo $form->errorSummary($contatoForm, '<div class="bg-primary" style="padding: 20px 20px 10px; margin: 20px 0;">', '</div>'); ?>

<div class="form-group">
	<?php echo $form->textField($contatoForm, 'nome', array(
		'class' => 'form-control',  
		'placeholder' => $contatoForm->getAttributeLabel('nome'),
	)); ?>
</div>

<div class="form-group">
	<?php echo $form->textField($contatoForm, 'telefone', array(
		'class' => 'form-control', 
		'placeholder' => $contatoForm->getAttributeLabel('telefone'),
	)); ?>
</div>

<div class="form-group">
	<?php echo $form->textField($contatoForm, 'email', array(
		'class' => 'form-control', 
		'placeholder' => $contatoForm->getAttributeLabel('email'),
	)); ?>
</div>

<div class="form-group">
	<?php echo $form->textArea($contatoForm, 'mensagem', array(
		'class' => 'form-control',
		'rows' => 4,
		'placeholder' => $contatoForm->getAttributeLabel('mensagem'),
	)); ?>
</div>

<div class="form-group text-right">
	<?php echo CHtml::button('Corrigir', array('type' => 'reset', 'class' => 'btn btn-primary btn-lg')); ?>
	<?php echo CHtml::button('Enviar', array('type' => 'submit', 'class' => 'btn btn-primary btn-lg')); ?>
</div>

<?php $this->endWidget(); ?>