<?php 
$categoryLinks = $data->getCategoryLinks(); 
?>

<article class="post">
	<header class="post-header">
		<h2 class="post-title"><?php echo CHtml::encode($data->title); ?></h2>	
		<div class="post-meta"> 
			<span class="post-author">Escrito por: <b><?php echo CHtml::encode($data->author->name); ?></b></span>
			<span class="post-create-time">em <b><date datetime="<?php echo date('Y-m-d', $data->create_time); ?>"><?php echo date('d/m/Y', $data->create_time); ?></date></b></span>
			<?php if ($data->update_time > $data->create_time) : ?>
			&ndash;
			<span class="post-update-time">Atualizado em: <date datetime="<?php echo date('Y-m-d', $data->update_time); ?>"><?php echo date('d/m/Y', $data->update_time); ?></date></span>
			<?php endif; ?>	
			<?php if ($categoryLinks) : ?>
			&ndash;
			<span class="post-category-links"> 
				Categorias: 
				<?php foreach ($categoryLinks as $categoryLink) : ?>
					<?php echo $categoryLink; ?>
				<?php endforeach; ?>
			</span>
			<?php endif; ?>
		</div>
	</header>
	<?php $file = $data->getFile('img', null, array('class' => 'post-img img-responsive')); ?>
	<?php echo $file ? $file->render : ''; ?>
	<div class="post-content">
		<?php echo $data->content; ?>
	</div>
</article>
