<?php $this->pageTitle = 'Bem-vindo'; ?>

<section class="main-section" id="posts">
	<div class="container">
		<div class="main-section-header">
			<h1 class="main-section-title">Notícias <span class="fa fa-angle-down"></span></h1>
			<hr>
		</div>
		<div class="row">
			<?php for ($i = 0; $i < 3; $i++) : ?>
			<div class="col col-md-4">
				<?php if (isset($posts[$i])) : ?>
				<?php $this->renderPartial('/post/_view', array('data' => $posts[$i], 'displayContent' => true)); ?>
				<?php endif; ?>
			</div>
			<?php endfor; ?>
		</div>
	</div>
</section>	

<section class="main-section" id="quem-somos">
	<div class="container">
		<div class="main-section-header">
			<h1 class="main-section-title">Quem Somos <span class="fa fa-angle-down"></span></h1>
			<hr>
		</div>
		<div class="row">
			<div class="col-md-5">
				<p><img class="img-responsive" src="img/palacio-da-justica.jpg"></p>
				<br>
				<blockquote>
					<p><span class="h4">"Tem fé no Direito como o melhor instrumento para a convivência humana. Na Justiça, como destino normal do Direito. Na Paz, como substituto bondoso da Justiça. E, sobretudo, tem fé na Liberdade, sem a qual não há Direito, nem Justiça, nem Paz"</span></p>
					<footer><cite title="Eduardo J. Couture">Eduardo J. Couture</cite></footer>
				</blockquote>
			</div>
			<div class="col-md-7">
				<p>Bicalho & Bicalho é uma sociedade de advogados do interior paulista, localizada em Salto, a 100 km da cidade de São Paulo, e registrada na OAB/SP sob o nº. 18.757.</p> 
				<p>O escritório é integrado por toda uma família de advogados, atuando há <?php echo (date('Y') - 1996); ?> anos nas diversas áreas do Direito, especialmente na Cível, Trabalhista, Sindical, Empresarial, Tributária e Previdenciária.</p>
				<p>Foi fundado pelo advogado Dr. Romeu Gonçalves Bicalho, em janeiro de 1996, e funciona à rua Prudente de Moraes, 793 – Centro – Salto.</p>
				<br><br class="visible-xs visible-sm">
				<div class="main-section-header">
					<h2 class="main-section-title">Área de Atuação</h2>
					<hr>
				</div>
				<div class="row text-uppercase">
					<div class="col-xs-6">
						<ul>
							<li>Direito Civil</li>
							<li>Direito de Família</li>
							<li>Direito Empresarial</li>
						</ul>
					</div>
					<div class="col-xs-6">
						<ul>
							<li>Direito do Trabalho</li>
							<li>Direito do Consumidor</li>
							<li>Direito Previdenciário</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>	

<section class="main-section" id="corpo-juridico">
	<div class="container">
		<div class="main-section-header">
			<h1 class="main-section-title">Corpo Jurídico <span class="fa fa-angle-down"></span></h1>
			<hr>
		</div>
		<div class="row">
			<div class="col-md-5">
				<div class="media">
					<img class="pull-left" src="img/dr-romeu.jpg" alt="Dr. Romeu Gonçalves Bicalho">
					<div class="media-body">
						<h5>Dr. Romeu Gonçalves Bicalho</h5>
						<p>Advogado inscrito na OAB/SP sob nº. 138.816. Graduado em Direito pela Faculdade de Direito de Itu – FADITU.</p>
						<p>Doutor em Direito pela Pontifícia Universidade Católica de São Paulo - PUC-SP. Mestre em Direito pela Universidade Metodista de Piracicaba - UNIMEP.</p>
						<p>Co-autor das obras "Questões de Exame de Ordem - comentadas e "A Nova Execução de Títulos Executivos Extrajudiciais - análise prática", ambas da editora Método e de "Ensaios de Direito Internacional – Fundamentos, Novos Atores e Integração Regional ", pela editora Millennium.</p>
						<p>Acesse mais informações: <a href="http://lattes.cnpq.br/7623585991411751">http://lattes.cnpq.br/7623585991411751</a></p>
					</div>	
				</div>
				<hr class="visible-xs visible-sm">
			</div>
			<div class="col-md-7">
				<div class="media">
					<img class="pull-left" src="img/dr-ronaldo.jpg" alt="Dr. Ronaldo Gonçalves Bicalho">
					<div class="media-body">
						<h5>Dr. Ronaldo Gonçalves Bicalho</h5>
						<p>Advogado inscrito na OAB/SP sob no. 211.865. Formado pela Faculdade de Direito de Itu- FADITU. Especialista em Direto do Trabalho pela Faculdade de Direito de Itu - FADITU.</p>
					</div>
				</div>
				<hr>
				<div class="media">
					<img class="pull-left" src="img/dra-jane.jpg" alt="Dra. Jane Gonçalves Bicalho Agostinho">
					<div class="media-body">
						<h5>Dra. Jane Gonçalves Bicalho Agostinho</h5>
						<p>Advogada inscrita na OAB/SP sob no. 253.652. Formada pela Faculdade de Direito de Itu – FADITU. Especialista em Direito Previdenciário.</p>
					</div>
				</div>
				<hr>
				<div class="media">
					<img class="pull-left" src="img/dra-mirna.jpg" alt="Dra. Mirna Maria Scalet Bicalho">
					<div class="media-body">
						<h5>Dra. Mirna Maria Scalet Bicalho</h5>
						<p>Advogada inscrita na OAB/SP sob no. 276.335. Graduada em Direito pela Faculdade de Direito de Itu - FADITU e em Jornalismo pela Universidade Metodista de Piracicaba – UNIMEP.</p>
					</div>
				</div>
				<hr>
				<div class="media">
					<img class="pull-left" src="img/dra-patricia.jpg" alt="Dra. Patrícia Gonçalves Bicalho">
					<div class="media-body">
						<h5>Dra. Patrícia Gonçalves Bicalho</h5>
						<p>Advogada inscrita na OAB/SP sob no. 313.924. Formada pela Faculdade de Direito de Itu – FADITU.</p>
					</div>
				</div>
			</div>					
		</div>
	</div>
</section>	