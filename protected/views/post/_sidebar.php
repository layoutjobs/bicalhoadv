<aside class="sidebar-aside">
	<h3 class="sidebar-aside-title">Pesquisar</h3>
	<form action="<?php echo $this->createUrl('/post'); ?>">
		<div class="input-group input-group-sm">
			<input type="text" class="form-control" name="s" value="<?php echo Yii::app()->request->getParam('s'); ?>" placeholder="Pesquisar..." autocomplete="false">
			<span class="input-group-btn">
				<button class="btn btn-default btn-primary" type="submit">Ir</button>
			</span>
		</div>
	</form>
</aside>

<aside class="sidebar-aside">
	<h3 class="sidebar-aside-title">Categorias</h3>
	<?php $this->renderCategoryNav(); ?>
</aside>

<aside class="sidebar-aside">
	<h3 class="sidebar-aside-title">Arquivo</h3>
	<?php $this->renderArchiveNav(); ?>
</aside>