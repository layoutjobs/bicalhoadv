<?php 
$this->pageTitle = CHtml::encode($post->title);
?>

<?php $this->renderPartial('_view', array('data' => $post, 'displayContent' => true, 'displayFullContent' => true, 'displayImg' => true, 'hideTitle' => true)); ?>