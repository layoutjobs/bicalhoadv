<img class="pull-right" src="<?php echo Yii::app()->baseUrl; ?>/img/banners/fique-por-dentro.png">
<div class="full-banner-body">
	<h1 class="full-banner-heading">Fique por dentro</h1>
	<p class="lead">Notícias de mercado, dicas, matérias e muitos outros assuntos relacionados a construção, decoração e reforma.</p>
</div>