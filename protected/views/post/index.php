<?php 
if (Yii::app()->request->getParam('category'))
	$contentTitle = Category::unfriendlyName(Yii::app()->request->getParam('category'));
else if (Yii::app()->request->getParam('year') && Yii::app()->request->getParam('month'))
	$contentTitle = 'Arquivo de "' . Yii::app()->request->getParam('year') . '/' . Yii::app()->request->getParam('month') . '"';
else if (Yii::app()->request->getParam('s'))
	$contentTitle = 'Resultados da busca por "' . Yii::app()->request->getParam('s') . '"';
else
	$contentTitle = ''; 

$this->pageTitle = 'Artigos' . ($contentTitle ? ' - ' . $contentTitle : '');
$this->sidebar = $this->renderPartial('_sidebar', array(), true);
?>

<?php $this->widget('zii.widgets.CListView', array(
	'id' => 'post-list',
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
	'template' => "{items}\n{pager}",
	'pagerCssClass' => 'post-pager',
	'pager' => array(
		'header' => false,
		'prevPageLabel' => 'Mais recentes &rarr;',
		'nextPageLabel' => '&larr; Mais antigos',
		'previousPageCssClass' => 'next',
		'nextPageCssClass' => 'previous',
		'selectedPageCssClass' => 'active',
		'lastPageCssClass' => 'hidden',
		'firstPageCssClass' => 'hidden',
		'hiddenPageCssClass' => 'disabled',
		'htmlOptions' => array('class' => 'pager'),
	),
	'htmlOptions' => array(
		'class' => 'post-group'
	),
)); ?>
