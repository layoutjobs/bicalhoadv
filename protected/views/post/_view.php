<?php 
$categoryLinks = $data->getCategoryLinks(); 
$readMoreUrl = Yii::app()->createUrl('/post/view', array('id' => $data->name));
$readMoreLink = CHtml::link('[ + ]', $readMoreUrl, array(
	'title' => 'Continue lendo (' . CHtml::encode($data->title) . ')',
	'rel' => 'tooltip',
	'class' => 'read-more',
));	
?>

<article class="post">
	<header class="post-header">
		<?php if (!isset($hideTitle)) : ?>

		<?php if (isset($displayFullContent)) : ?>
		<h2 class="post-title"><?php echo CHtml::encode($data->title); ?></h2>	
		<?php else : ?>
		<h2 class="post-title">
			<a href="<?php echo $readMoreUrl; ?>">
				<?php echo CHtml::encode($data->title); ?>	
				<?php if (!isset($displayContent)) : ?>
				<i class="fa fa-external-link"></i>
				<?php endif; ?>
			</a>
		</h2>
		<?php endif; ?>

		<?php endif; ?>

		<div class="post-meta"> 
			<span class="post-author">Escrito por: <b><?php echo CHtml::encode($data->author->name); ?></b></span>
			<span class="post-create-time">em <b><date datetime="<?php echo date('Y-m-d', $data->create_time); ?>"><?php echo date('d/m/Y', $data->create_time); ?></date></b></span>
			<?php if ($data->update_time > $data->create_time) : ?>
			&ndash;
			<span class="post-update-time">Atualizado em: <date datetime="<?php echo date('Y-m-d', $data->update_time); ?>"><?php echo date('d/m/Y', $data->update_time); ?></date></span>
			<?php endif; ?>	
			<?php if ($categoryLinks) : ?>
			&ndash;
			<span class="post-category-links"> 
				Categorias: 
				<?php foreach ($categoryLinks as $categoryLink) : ?>
					<?php echo $categoryLink; ?>
				<?php endforeach; ?>
			</span>
			<?php endif; ?>
		</div>
	</header>

	<?php if (isset($displayImg)) : ?>
		
	<?php $file = $data->getFile('img', null, array('class' => 'post-img img-responsive')); ?>
	<?php echo $file ? $file->render : ''; ?>

	<?php endif; ?>

	<?php if (isset($displayFullContent)) : ?>

	<div class="post-content">
		<?php echo $data->content; ?>
	</div>

	<?php elseif (isset($displayContent)) : ?>

	<div class="post-excerpt">
		<?php $content = ''; ?>

		<?php $content = $this->widget('ext.XReadMore', array(
			'model' => $data,
			'attribute' => 'content',
			'maxChar' => 400,
			'showLink' => false,
		), true); ?>

		<?php $content .= ' ' . $readMoreLink; ?>	

		<?php echo $content; ?>
	</div>

	<?php endif; ?>
</article>
