<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo CHtml::encode(Yii::app()->params['meta.description']); ?>">
	<meta name="author" content="<?php echo CHtml::encode(Yii::app()->params['meta.author']); ?>">
	<title><?php echo CHtml::encode(Yii::app()->name . ' - ' . $this->pageTitle); ?></title>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>


	<!-- HEADER
	================================================== -->

	<header class="main-header">
		<div class="container">
			<a href="<?php echo Yii::app()->homeUrl; ?>">
				<img class="img-responsive center-block" src="<?php echo Yii::app()->baseUrl; ?>/img/logo.png" width="390" height="165" alt="<?php echo Yii::app()->name; ?>">
			</a>
		</div>
	</header>


	<!-- CAROUSEL
	================================================== -->

	<?php if ($this->id === 'home' && $this->action->id === 'index') : ?>

	<section class="main-banner">
		<div id="mainCarousel" class="carousel" data-ride="carousel">
			<div class="carousel-inner">
				<div class="item">
					<img src="<?php echo Yii::app()->baseUrl; ?>/img/banner-1.jpg" width="890" height="350">
					<div class="carousel-caption">Confiança</div>
				</div>
				<div class="item active">
					<img src="<?php echo Yii::app()->baseUrl; ?>/img/banner-2.jpg" width="890" height="350">
					<div class="carousel-caption">Competência</div>
				</div>
				<div class="item">
					<img src="<?php echo Yii::app()->baseUrl; ?>/img/banner-3.jpg" width="890" height="350">
					<div class="carousel-caption">Ética</div>
				</div>
			</div>
			<a class="left carousel-control" href="#mainCarousel" data-slide="prev"><span class="fa fa-angle-left"></span></a>
			<a class="right carousel-control" href="#mainCarousel" data-slide="next"><span class="fa fa-angle-right"></span></a>
		</div>
	</section>

	<?php endif; ?>


	<!-- NAV
	================================================== -->

	<nav class="main-nav">
		<div class="container">
			<ul class="nav nav-justified">
				<li><a href="<?php echo Yii::app()->homeUrl; ?>#quem-somos">Quem Somos <span class="fa fa-angle-down"></span></a></li>
				<li><a href="<?php echo Yii::app()->homeUrl; ?>#corpo-juridico">Corpo Jurídico <span class="fa fa-angle-down"></span></a></li>
				<li><a href="<?php echo Yii::app()->createUrl('/post'); ?>">Artigos <span class="fa fa-angle-down"></span></a></li>
				<li><a href="#contato">Contato <span class="fa fa-angle-down"></span></a></li>	
			</ul>	
		</div>	
	</nav>

	<?php if (Yii::app()->user->hasFlash('success') || Yii::app()->user->hasFlash('error')) : ?>

	<section class="main-section text-center bg-primary">
		<?php if (Yii::app()->user->hasFlash('error', false)) : ?>
			<span class="fa fa-times"></span> <?php echo Yii::app()->user->getFlash('error'); ?>
		<?php elseif (Yii::app()->user->hasFlash('success', false)) : ?>
			<span class="fa fa-check"></span> <?php echo Yii::app()->user->getFlash('success'); ?>
		<?php endif; ?>
	</section>

	<?php endif; ?>

	<?php if ($this->id === 'home' && $this->action->id === 'index') : ?>

	<?php echo $content; ?>

	<?php else : ?>

	<section class="main-section">
		<div class="container">
			<?php if ($this->sidebar) : ?>

			<div class="row">
				<div class="col-md-8">
					<?php if (!$this->hideHeader) : ?>

					<div class="main-section-header">
						<h1 class="main-section-title"><?php echo CHtml::encode($this->pageTitle); ?> <i class="fa fa-angle-down"></i></h1>
						<hr>
					</div>

					<?php endif; ?>

					<?php echo $content; ?>
				</div>
				<div class="col-md-4">
					<div class="sidebar">
						<?php echo $this->sidebar; ?>
					</div>
				</div>
			</div>

			<?php else : ?>

			<?php if (!$this->hideHeader) : ?>

			<div class="main-section-header">
				<h1 class="main-section-title"><?php echo CHtml::encode($this->pageTitle); ?> <i class="fa fa-angle-down"></i></h1>
				<hr>
			</div>

			<?php endif; ?>

			<?php echo $content; ?>

			<?php endif; ?>
		</div>
	</section>	

	<?php endif; ?>


	<?php if ($this->id !== 'home' || ($this->id === 'home' && $this->action->id !== 'contato')) : ?>

	<!-- CONTATO
	================================================== -->

	<section class="main-section main-section-bg" id="contato">
		<div class="container">
			<div class="row">
				<div class="col-1 col-md-6">
					<h1 class="main-section-title">Como Chegar <span class="fa fa-angle-down"></span></h1>
					<div id="map" style="width: 100%; height: 230px;"></div><br><br>
					<address class="text-uppercase">
						<span class="h4">Rua Prudente de Moraes, 793<br>Salto/SP - Cep 13.320-160</span>
						<div class="media h2">
							<span class="pull-left">Fones:</span>
							<div class="media-body">
								<strong>(11) 4029.4703</strong><br>
								<strong>(11) 4029.2996</strong>
							</div>
						</div>
						<span class="h4">E-mail: <a href="bicalho.secretaria@adv.oabsp.org.br">bicalho.secretaria@adv.oabsp.org.br</a></span>
					</address>
				</div>
				<br class="visible-sm visible-xs">
				<div class="col-2 col-md-6">
					<h1 class="main-section-title">Fale Conosco <span class="fa fa-angle-down"></span></h1>
					<?php echo $this->renderPartial('/home/_contatoForm'); ?>
				</div>
			</div>			
		</div>
	</section>	

	<?php endif; ?>


	<!-- FOOTER
	================================================== -->

	<footer class="main-footer text-center">
		<div class="container">
			<img src="<?php echo Yii::app()->baseUrl; ?>/img/logo.png" width="234" height="99" alt="<?php echo Yii::app()->name; ?>">
			<p><a href="#top" class="h4 text-uppercase"><span class="fa fa-angle-up"></span><br><strong>Topo</strong></a></p>
		</div>
	</footer>


	<!-- AJAX-LOADING
	================================================== -->

	<div id="ajax-loading"></div>


	<!-- REGISTER CSS
	================================================== -->		

	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/bootstrap.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/font-awesome.min.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/main.css'); ?>


	<!-- REGISTER JS
	================================================== -->

	<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap.min.js', CClientScript::POS_END); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/main.js', CClientScript::POS_END); ?>

	<?php if (Yii::app()->params['map.lat'] && Yii::app()->params['map.lng']) : ?>

	<script src="https://maps.google.com/maps/api/js?sensor=true"></script>

	<script>
	function initialize() {
		var styles = [
			{
				stylers: [
					{ hue: "#150c09" },
					{ saturation: -10 }
				]
			},{
				featureType: "road",
				elementType: "geometry",
				stylers: [
					{ lightness: 40 },
					{ visibility: "simplified" }
				]
			},{
				featureType: "road",	
				elementType: "labels",
				stylers: [
					{ visibility: "off" }
				]
			}
		], 
		mapOptions = {
			zoom: 15, 
			center: new google.maps.LatLng(<?php echo Yii::app()->params['map.lat']; ?>, <?php echo Yii::app()->params['map.lng']; ?>),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		},
		map = new google.maps.Map(document.getElementById('map'), mapOptions),
		marker = new google.maps.Marker({
			position:  map.getCenter(), 
			map: map,
			title: '<?php echo Yii::app()->name; ?>',
			url: 'https://www.google.com.br/maps/preview#!q=<?php echo Yii::app()->params['map.lat'] . ',' . Yii::app()->params['map.lng']; ?>',
		}), 
		styledMap = new google.maps.StyledMapType(styles, { name: '<?php echo Yii::app()->name; ?>' });

		map.mapTypes.set('map_style', styledMap);
		map.setMapTypeId('map_style');

		google.maps.event.addListener(marker, 'click', function() {
			window.open(marker.url, '_blank');
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
	</script>

	<?php endif; ?>

	<?php if (Yii::app()->params['ga.trackingId']) : ?>

	<script>
		var _gaq=[['_setAccount','<?php echo Yii::app()->params['ga.trackingId']; ?>'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src='//www.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>

	<?php endif; ?>
</body>
</html>