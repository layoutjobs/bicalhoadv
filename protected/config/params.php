<?php

return array(
	'url.format' => 'path',
	'url.urlSuffix' => '/',
	'url.rules' => array(
		/* for REST please @see http://www.yiiframework.com/wiki/175/how-to-create-a-rest-api/ */
		/* other @see http://www.yiiframework.com/doc/guide/1.1/en/topics.url */
		'post/tag/<tag>' => 'post/index',
		'post/categoria/<category>' => 'post/index',
		'post/arquivo/<year>/<month>' => 'post/index',
		'post/<id>' => 'post/view',
	),
	'meta.author' => 'Layout - Criação e Publicidade - Salto/SP',
	'meta.description' => 'Bicalho & Bicalho é uma sociedade de advogados do interior paulista, localizada em Salto, a 100 km da cidade de São Paulo, e registrada na OAB/SP sob o nº. 11.790. O escritório é integrado por toda uma família de advogados, atuando há ' . (date('Y') - 1996) . ' anos nas diversas áreas do Direito, especialmente na Cível, Trabalhista, Sindical, Empresarial, Tributária e Previdenciária. Foi fundado pelo advogado Dr. Romeu Gonçalves Bicalho, em janeiro de 1996, e funciona à rua Prudente de Moraes, 793 – Centro – Salto.',
	'map.lat' => '-23.202819',
	'map.lng' => '-47.288866',
	'ga.trackingId' => '',
);