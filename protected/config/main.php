<?php

$base 		    = __DIR__ . '/..';
$common 		= __DIR__ . '/../../common';
$environment 	= APP_ENV;

$params         = require($base . '/config/params-' . $environment . '.php');
$commonParams   = require($common . '/config/params-' . $environment . '.php');

Yii::setPathOfAlias('common', $common);

return CMap::mergeArray(array(
	'name' 					=> 'Bicalho & Bicalho',
	'defaultController' 	=> 'home', 
	'language' 				=> 'pt_br',
	'timeZone'              => 'America/Sao_Paulo',
	
	'preload' => array(
		'log',
	),
	
	'import' => array(
        'common.components.*',
        'common.extensions.*',
        'common.models.*',
        'application.components.*',
        'application.extensions.*',
        'application.models.*',
	),
	
	'modules' => array(

	),
	
	'components' => array(
		'coreMessages' => array(
			'basePath' => $common . '/messages',
		),
		'db' => array(
			'connectionString' => 'sqlite:common/data/database.db',
			'tablePrefix' => 'tbl_',
		),
		'urlManager' => array(
			'urlFormat' => $params['url.format'],
			'rules' => $params['url.rules'],
			'urlSuffix' => $params['url.urlSuffix'],
			'showScriptName' => false, // requer .httacess
		),
		'errorHandler' => array(
			'errorAction' => 'home/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),

	'params' => CMap::mergeArray($commonParams, $params),
), require($base . '/config/main-' . $environment . '.php'));