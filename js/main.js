!function($) {

  "use strict";

  var oldIdx,
      oldItem,
      newIdx,
      newItem;

  $('#mainCarousel').on('slide.bs.carousel', function (e) {
    oldIdx  = $('.active', e.target).index();
    oldItem = $('#mainCarousel .item:nth-child(' + oldIdx + ')');
  });

  $('#mainCarousel').on('slid.bs.carousel', function (e) {
    newIdx  = $('.active', e.target).index();
    newItem = $('#mainCarousel .item:nth-child(' + newIdx + ')');

    if (newIdx > oldIdx)
      $('#mainCarousel .item:first').appendTo($('#mainCarousel .carousel-inner'));
    else
      $('#mainCarousel .item:last').prependTo($('#mainCarousel .carousel-inner'));
  }); 

  $('.nav > li > a').each(function() {
    if (typeof $(this.hash).get(0) != 'undefined') {
      $(this).on('click', function(e) {
        $('html, body').animate({
          scrollTop: $(this.hash).offset().top
        }, 1000);
      });
    }
  });

  $('[href^="#top"]').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 1000);
  });

}(window.jQuery);