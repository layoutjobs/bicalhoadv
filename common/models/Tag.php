<?php

class Tag extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_tag':
	 * @var integer $id
	 * @var string $name
	 * @var string $model
	 * @var string $title
	 * @var integer $frequency
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table title
	 */
	public function tableName()
	{
		return '{{tag}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('frequency', 'numerical', 'integerOnly'=>true),
			array('name, title', 'length', 'max'=>128),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation title and the related
		// class title for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (title=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'name' => 'Name',
			'title' => 'Title',
			'model' => 'Model',
			'frequency' => 'Frequency',
		);
	}


	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->name=$this->friendlyName($this->title);
			return true;
		}
		else
			return false;
	}

	public static function friendlyName($name) 
	{
		// replace non letter or digits by -
		$name = preg_replace('~[^\\pL\d]+~u', '-', $name);
		// trim
		$name = trim($name, '-');
		// transliterate
		$name = iconv('utf-8', 'us-ascii//TRANSLIT', $name);
		// lowercase
		$name = strtolower($name);
		// remove unwanted characters and return
		return preg_replace('~[^-\w]+~', '', $name);
	}

	public static function unfriendlyName($name) 
	{
		$tag = self::model()->findByAttributes(array('name' => $name));
		if ($tag)
			return CHtml::encode($tag->title);
		else
			return '';
	}

	/**
	 * Returns tag titles and their corresponding weights.
	 * Only the tags with the top weights will be returned.
	 * @param integer the maximum number of tags that should be returned
	 * @return array weights indexed by tag titles.
	 */
	public function findTagWeights($limit=20)
	{
		$models=$this->findAll(array(
			'order'=>'frequency DESC',
			'limit'=>$limit,
		));

		$total=0;
		foreach($models as $model)
			$total+=$model->frequency;

		$tags=array();
		if($total>0)
		{
			foreach($models as $model)
				$tags[$model->title]=8+(int)(16*$model->frequency/($total+10));
			ksort($tags);
		}
		return $tags;
	}

	/**
	 * Suggests a list of existing tags matching the specified keyword.
	 * @param string the tag model
	 * @param string the keyword to be matched
	 * @param integer maximum number of tags to be returned
	 * @return array list of matching tag titles
	 */
	public function suggestTags($model,$keyword,$limit=20)
	{
		$tags=$this->findAll(array(
			'condition'=>'title LIKE :keyword AND model = :model',
			'order'=>'frequency DESC, title',
			'limit'=>$limit,
			'params'=>array(
				':keyword'=>'%'.strtr($keyword,array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')).'%',
				':model'=>$model,
			),
		));
		$titles=array();
		foreach($tags as $tag)
			$titles[]=$tag->title;
		return $titles;
	}

	public static function string2array($tags)
	{
		return preg_split('/\s*,\s*/',trim($tags),-1,PREG_SPLIT_NO_EMPTY);
	}

	public static function array2string($tags)
	{
		return implode(', ',$tags);
	}

	public function updateFrequency($model, $oldTags, $newTags)
	{
		$oldTags=self::string2array($oldTags);
		$newTags=self::string2array($newTags);
		$this->addTags($model,array_values(array_diff($newTags,$oldTags)));
		$this->removeTags($model,array_values(array_diff($oldTags,$newTags)));
	}

	public function addTags($model, $tags)
	{
		$criteria=new CDbCriteria;
		$criteria->compare('model',$model);
		$criteria->addInCondition('title',$tags);
		$this->updateCounters(array('frequency'=>1),$criteria);
		foreach($tags as $title)
		{
			if(!$this->exists('title=:title AND model=:model',array(':title'=>$title,':model'=>$model)))
			{
				$tag=new Tag;
				$tag->title=$title;
				$tag->frequency=1;
				$tag->model=$model;
				$tag->save();
			}
		}
	}

	public function removeTags($model, $tags)
	{
		if(empty($tags))
			return;
		$criteria=new CDbCriteria;
		$criteria->compare('model',$model);
		$criteria->addInCondition('title',$tags);
		$this->updateCounters(array('frequency'=>-1),$criteria);
		$this->deleteAll('frequency<=0');
	}
}