<?php

class Category extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_category':
	 * @var integer $id
	 * @var string $name
	 * @var string $model
	 * @var string $title
	 * @var integer $frequency
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table title
	 */
	public function tableName()
	{
		return '{{category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('frequency', 'numerical', 'integerOnly'=>true),
			array('name, title', 'length', 'max'=>128),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation title and the related
		// class title for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (title=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'name' => 'Name',
			'title' => 'Title',
			'model' => 'Model',
			'frequency' => 'Frequency',
		);
	}


	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->name=$this->friendlyName($this->title);
			return true;
		}
		else
			return false;
	}

	public static function friendlyName($name) 
	{
		// replace non letter or digits by -
		$name = preg_replace('~[^\\pL\d]+~u', '-', $name);
		// trim
		$name = trim($name, '-');
		// transliterate
		$name = iconv('utf-8', 'us-ascii//TRANSLIT', $name);
		// lowercase
		$name = strtolower($name);
		// remove unwanted characters and return
		return preg_replace('~[^-\w]+~', '', $name);	
	}

	public static function unfriendlyName($name) 
	{
		$category = self::model()->findByAttributes(array('name' => $name));
		if ($category)
			return CHtml::encode($category->title);
		else
			return '';
	}

	/**
	 * Returns category titles and their corresponding weights.
	 * Only the categories with the top weights will be returned.
	 * @param integer the maximum number of categories that should be returned
	 * @return array weights indexed by category titles.
	 */
	public function findCategoryWeights($limit=20)
	{
		$models=$this->findAll(array(
			'order'=>'frequency DESC',
			'limit'=>$limit,
		));

		$total=0;
		foreach($models as $model)
			$total+=$model->frequency;

		$categories=array();
		if($total>0)
		{
			foreach($models as $model)
				$categories[$model->title]=8+(int)(16*$model->frequency/($total+10));
			ksort($categories);
		}
		return $categories;
	}

	/**
	 * Suggests a list of existing categories matching the specified keyword.
	 * @param string the category model
	 * @param string the keyword to be matched
	 * @param integer maximum number of categories to be returned
	 * @return array list of matching category titles
	 */
	public function suggestCategories($model,$keyword,$limit=20)
	{
		$categories=$this->findAll(array(
			'condition'=>'title LIKE :keyword AND model = :model',
			'order'=>'frequency DESC, title',
			'limit'=>$limit,
			'params'=>array(
				':keyword'=>'%'.strtr($keyword,array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')).'%',
				':model'=>$model,
			),
		));
		$titles=array();
		foreach($categories as $category)
			$titles[]=$category->title;
		return $titles;
	}

	public static function string2array($categories)
	{
		return preg_split('/\s*,\s*/',trim($categories),-1,PREG_SPLIT_NO_EMPTY);
	}

	public static function array2string($categories)
	{
		return implode(', ',$categories);
	}

	public function updateFrequency($model, $oldCategories, $newCategories)
	{
		$oldCategories=self::string2array($oldCategories);
		$newCategories=self::string2array($newCategories);
		$this->addCategories($model,array_values(array_diff($newCategories,$oldCategories)));
		$this->removeCategories($model,array_values(array_diff($oldCategories,$newCategories)));
	}

	public function addCategories($model, $categories)
	{
		$criteria=new CDbCriteria;
		$criteria->compare('model',$model);
		$criteria->addInCondition('title',$categories);
		$this->updateCounters(array('frequency'=>1),$criteria);
		foreach($categories as $title)
		{
			if(!$this->exists('title=:title AND model=:model',array(':title'=>$title,':model'=>$model)))
			{
				$category=new Category;
				$category->title=$title;
				$category->frequency=1;
				$category->model=$model;
				$category->save();
			}
		}
	}

	public function removeCategories($model, $categories)
	{
		if(empty($categories))
			return;
		$criteria=new CDbCriteria;
		$criteria->compare('model',$model);
		$criteria->addInCondition('title',$categories);
		$this->updateCounters(array('frequency'=>-1),$criteria);
		$this->deleteAll('frequency<=0');
	}
}