<?php

class Post extends CActiveRecord
{	
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $name
	 * @var string $title
	 * @var string $img
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */
	const STATUS_DRAFT=1;
	const STATUS_PUBLISHED=2;
	const STATUS_ARCHIVED=3;

	private $_oldTags;
	private $_oldCategories;

	public $s;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{post}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, content, status', 'required'),
			array('status', 'in', 'range'=>array(1,2,3)),
			array('title', 'length', 'max'=>128),
			// array('tags, categories', 'match', 'pattern'=>'/^[\w\s,]+$/', 'message'=>'{attribute} devem conter apenas letras, espaços e números.'),
			array('content', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
			array('tags', 'normalizeTags'),
			array('categories', 'normalizeCategories'),
			array('name, title', 'unique'),

			array('s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => '#',
			'name' => 'Nome',
			'title' => 'Título',
			'img' => 'Imagem',
			'content' => 'Conteúdo',
			'tags' => 'Tags',
			'categories' => 'Categorias',
			'status' => 'Status',
			'create_time' => 'Criado em',
			'update_time' => 'Atualizado em',
			'author_id' => 'Autor',
		);
	}

	public function behaviors()
	{
		return array(
			'FileBehavior' => array(
				'class' => 'common.behaviors.FileBehavior',
				'uploadFolder' => 'post',
				'uploadModelFolder' => str_pad($this->primaryKey, 10, '0', STR_PAD_LEFT),
				'uploadScopes' => array(
					'img' => array(
						//'folder' => 'imgs', 
						'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
						'attribute' => 'img',
					),
				),
			),
		); 
	}

	/**
	 * @return string the URL that shows the detail of the post
	 */
	public function getUrl()
	{
		return Yii::app()->createUrl('post/view', array(
			'name'=>$this->name,
		));
	}

	/**
	 * @return array a list of links that point to the post list filtered by every tag of this post
	 */
	public function getTagLinks()
	{
		$links=array();
		foreach(Tag::string2array($this->tags) as $tag)
			$links[]=CHtml::link(CHtml::encode($tag), array('post/index', 'tag'=>Tag::friendlyName($tag)));
		return $links;
	}

	/**
	 * @return array a list of links that point to the post list filtered by every category of this post
	 */
	public function getCategoryLinks($htmlOptions = null)
	{
		$links=array();
		foreach(Category::string2array($this->categories) as $category)
			$links[]=CHtml::link(CHtml::encode($category), 
				array('post/index', 'category'=>Category::friendlyName($category)), $htmlOptions);
		return $links;
	}

	/**
	 * Normalizes the user-entered tags.
	 */
	public function normalizeTags($attribute,$params)
	{
		$this->tags=Tag::array2string(array_unique(Tag::string2array($this->tags)));
	}

	/**
	 * Normalizes the user-entered categories.
	 */
	public function normalizeCategories($attribute,$params)
	{
		$this->categories=Category::array2string(array_unique(Category::string2array($this->categories)));
	}

	/**
	 * Adds a new comment to this post.
	 * This method will set status and post_id of the comment accordingly.
	 * @param Comment the comment to be added
	 * @return boolean whether the comment is saved successfully
	 */
	public function addComment($comment)
	{
		if(Yii::app()->params['commentNeedApproval'])
			$comment->status=Comment::STATUS_PENDING;
		else
			$comment->status=Comment::STATUS_APPROVED;
		$comment->post_id=$this->id;
		return $comment->save();
	}

	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
	protected function afterFind()
	{
		parent::afterFind();
		$this->_oldTags=$this->tags;
		$this->_oldCategories=$this->categories;
	}

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->create_time=$this->update_time=time();
				$this->author_id=Yii::app()->user->id;
			}
			else
				$this->update_time=time();

			$this->name = preg_replace('~[^\\pL\d]+~u', '-', $this->title);
			$this->name = trim($this->name, '-');
			$this->name = iconv('utf-8', 'us-ascii//TRANSLIT', $this->name);
			$this->name = strtolower($this->name);
			$this->name = preg_replace('~[^-\w]+~', '', $this->name);

			return true;
		}
		else
			return false;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		Tag::model()->updateFrequency('Post', $this->_oldTags, $this->tags);
		Category::model()->updateFrequency('Post', $this->_oldCategories, $this->categories);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency('Post', $this->tags, '');
		Category::model()->updateFrequency('Post', $this->categories, '');
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		if ($this->s) {
			$criteria->compare('title', $this->s, true);
			$criteria->compare('content', $this->s, true, 'OR');			
			$criteria->compare('categories', $this->s, true, 'OR');			
			$criteria->compare('tags', $this->s, true, 'OR');			
		}

		return new CActiveDataProvider('Post', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'status, update_time DESC',
			),
		));
	}
}