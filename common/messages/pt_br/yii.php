<?php
/**
 * Message translations.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return CMap::mergeArray(require(Yii::getFrameworkPath() . '/messages/pt_br/yii.php'), array( 
 'The format of {attribute} is invalid.' => '{attribute}: formato inválido.',
  '{attribute} "{value}" has already been taken.' => '{attribute}: "{value}" já existe, e não pode se repetir.',
  '{attribute} "{value}" is invalid.' => '{attribute}: "{value}" é inválido.',
  '{attribute} cannot accept more than {limit} files.' => '{attribute}: não aceita mais que {limit} arquivo(s).',
  '{attribute} cannot be blank.' => '{attribute}: não pode ser vazio.',
  '{attribute} is in the list.' => '{attribute}: está na lista.',
  '{attribute} is invalid.' => '{attribute}: inválido.',
  '{attribute} is not a valid URL.' => '{attribute}: não é uma URL válida.',
  '{attribute} is not a valid email address.' => '{attribute}: não é um endereço de e-mail válido.',
  '{attribute} is not in the list.' => '{attribute}: não está na lista.',
  '{attribute} is of the wrong length (should be {length} characters).' => '{attribute}: está com tamanho errado (deve ter {length} caracteres).',
  '{attribute} is too big (maximum is {max}).' => '{attribute}: muito grande (máximo {max}).',
  '{attribute} is too long (maximum is {max} characters).' => '{attribute}: muito longo (máximo {max} caracteres).',
  '{attribute} is too short (minimum is {min} characters).' => '{attribute}: muito curto (mínimo {min} caracteres).',
  '{attribute} is too small (minimum is {min}).' => '{attribute}: é muito pequeno (mínimo {min}).',
  '{attribute} must be a number.' => '{attribute}: deve ser um número.',
  '{attribute} must be an integer.' => '{attribute}: deve ser um inteiro.',
  '{attribute} must be either {true} or {false}.' => '{attribute}: deve ser {true} ou {false}.',
  '{attribute} must be greater than "{compareValue}".' => '{attribute}: deve ser maior que "{compareValue}".',
  '{attribute} must be greater than or equal to "{compareValue}".' => '{attribute}: deve ser maior que ou igual a "{compareValue}".',
  '{attribute} must be less than "{compareValue}".' => '{attribute}: deve ser menor que "{compareValue}".',
  '{attribute} must be less than or equal to "{compareValue}".' => '{attribute}: deve ser menor que ou igual a "{compareValue}".',
  '{attribute} must be repeated exactly.' => '{attribute}: deve ser exatamente repetido.',
  '{attribute} must be {type}.' => '{attribute}: deve ser {type}.',
  '{attribute} must be {value}.' => '{attribute}: deve ser {value}.',
  '{attribute} must not be equal to "{compareValue}".' => '{attribute}: não deve ser igual a "{compareValue}".',
));
