<?php

// defined('YII_DEBUG') or define('YII_DEBUG', true);
// defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

chdir(__DIR__);
date_default_timezone_set('America/Sao_Paulo');

$environment = strpos($_SERVER['SERVER_NAME'], '.') !== false ? 'web' : 'local';
defined('APP_ENV') or define('APP_ENV', $environment);

require_once('common/vendor/yii/framework/yii.php');

Yii::createWebApplication('protected/config/main.php')->run();